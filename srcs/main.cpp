#include <iostream>
#include <ctime>
#include <NTL/ZZ.h>
#include <NTL/vector.h>
#include <algorithm> //for std::generate_n
#include <cassert>

#include "rsa.hpp"

std::string random_string(size_t length)
{
    auto randchar = []() -> char {
        const char charset[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[rand() % max_index];
    };
    std::string str(length, 0);
    std::generate_n(str.begin(), length, randchar);
    return str;
}

void record(int key_size, size_t message_len, int version)
{

    NTL::Vec<NTL::ZZ> cryto;
    clock_t start_gen, start_enc, start_dec;
    double time_gen, time_enc, time_dec;
    start_gen = clock();
    const KeyPair kp = RSA::genKeyPair(key_size, version);
    time_gen = (double)(clock() - start_gen) / CLOCKS_PER_SEC;

    std::string src_msg = random_string(message_len), dest_msg;
    std::cout << "Key size: " << key_size << ", String length: " << message_len << std::endl;
    std::cout << "Source message: " << src_msg << std::endl;

    vector<uint8_t> src_msg_vec(src_msg.begin(), src_msg.end());
    std::cout << "Start Encrypting..." << std::endl;
    start_enc = clock();
    vector<uint8_t> encrypted_message = RSA::doEncrypt(kp.pubKey, src_msg_vec, PaddingMode::PKCS1);
    time_enc = (double)(clock() - start_enc) / CLOCKS_PER_SEC;

    std::cout << "Start Decrypting..." << std::endl;
    start_dec = clock();
    vector<uint8_t> decrypted_message = RSA::doDecrypt(kp.pvtKey, encrypted_message, PaddingMode::PKCS1);
    dest_msg.assign(decrypted_message.begin(), decrypted_message.end());
    time_dec = (double)(clock() - start_enc) / CLOCKS_PER_SEC;

    assert(src_msg.compare(dest_msg) == 0);

    std::cout << "Time Gen Prime: " << time_gen << std::endl;
    std::cout << "Time Encrypt: " << time_enc << std::endl;
    std::cout << "Time Decrypt: " << time_dec << std::endl;
    std::cout << "=======================================" << std::endl;
}

void input(int argc, char **argv)
{

    const int NBITS = 2048;

    const KeyPair kp = RSA::genKeyPair(NBITS, (int)(argv[1][0] - '0'));

    std::string st;
    std::cout << "Enter a string: ";
    std::cin >> st;
    vector<uint8_t> in_vec(st.begin(), st.end());
    vector<uint8_t> encrypted_message = RSA::doEncrypt(kp.pubKey, in_vec, PaddingMode::PKCS1);
    vector<uint8_t> decrypted_message = RSA::doDecrypt(kp.pvtKey, encrypted_message, PaddingMode::PKCS1);
    string ret_str;
    ret_str.assign(decrypted_message.begin(), decrypted_message.end());
    std::cout << "Plain text : " << ret_str << std::endl;
}

void benchmark(int argc, char **argv)
{
    int version = (int)(argv[1][0] - '0');
    if (version != 2)
    {
        for (int i = 8; i < 12; i++)
        {
            record(1 << i, 100, version);
            record(1 << i, 1000, version);
        }
    }
    else if (version == 2)
    {
        for (int i = 4; i <= 5; i++)
        {
            record(1 << i, 100, version);
            record(1 << i, 1000, version);
        }
    } 
}

int main(int argc, char **argv)
{
    benchmark(argc, argv);
    return 0;
}