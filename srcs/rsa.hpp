#include <NTL/ZZ.h>
#include <NTL/vector.h>

#include <string>
#include <iostream>
#include <stdint.h>
#include <limits.h>
#include <algorithm> // for copy() and assign()
#include <vector>
#include <sstream>
#include "bigMul.hpp"

using namespace std;
using namespace NTL;

#ifndef RSA_H
#define RSA_H

#define ERR 80

// #define DEBUG_LIB

enum PaddingMode
{
    PKCS1 = 0,
};

struct PvtKey
{
    NTL::ZZ d;
    NTL::ZZ n;
};

struct PubKey
{
    NTL::ZZ e;
    NTL::ZZ n;
};

struct KeyPair
{
    PubKey pubKey;
    PvtKey pvtKey;
};

long get_len(NTL::ZZ a)
{
    return (long)ceil(log(a + 1) / log(10));
}

void reverse_array(unsigned char *array, long size)
{
    int temp = 0;
    for (int i = 0; i < size / 2; i++)
    {
        temp = array[i];
        array[i] = array[size - 1 - i];
        array[size - 1 - i] = temp;
    }
}

class RSA
{
public:
    static NTL::ZZ gcd(NTL::ZZ a, NTL::ZZ b)
    {
        NTL::ZZ temp;
        while (true)
        {
            temp = a % b;
            if (temp == 0)
                return b;
            a = b;
            b = temp;
        }
    }

    static NTL::ZZ powerIngeter(NTL::ZZ baseNumber, int times)
    {
        NTL::ZZ result = NTL::ZZ(1);
        while (times > 0)
        {
            if (times % 2 == 1)
            {
                result = result * baseNumber;
            }
            baseNumber = baseNumber * baseNumber;
            times = times >> 1;
        }
        return result;
    }

    static NTL::ZZ powerModBigIngeter(NTL::ZZ baseNumber, NTL::ZZ times, NTL::ZZ modBase)
    {
        NTL::ZZ result = NTL::ZZ(1);
        while (times > 0)
        {
            if (times % 2 == 1)
            {
                result = (result * baseNumber) % modBase;
            }
            baseNumber = (baseNumber * baseNumber) % modBase;
            times = times >> 1;
        }
        return result;
    }

    static long witness(const NTL::ZZ &n, const NTL::ZZ &x)
    {
        NTL::ZZ m, y, z;
        long j, k;

        if (x == 0)
            return 0;
        // compute m, k such that n-1 = 2^k * m, m odd:

        k = 1;
        m = n / 2;
        while (m % 2 == 0)
        {
            k++;
            m /= 2;
        }

        z = powerModBigIngeter(x, m, n); // z = x^m % n
        if (z == 1)
            return 0;

        j = 0;
        do
        {
            y = z;
            z = (y * y) % n;
            j++;
        } while (j < k && z != 1);

        return z != 1 || y != n - 1;
    }

    static long PrimeTest(const NTL::ZZ &n, long t)
    {
        if (n <= 1)
            return 0;

        // first, perform trial division by primes up to 2000
        NTL::ZZ timeSet;
        conv(timeSet, (long)time(0));
        NTL::SetSeed(timeSet);

        NTL::PrimeSeq s; // a class for quickly generating primes in sequence
        long p;

        p = s.next(); // first prime is always 2
        while (p && p < 2000)
        {
            if ((n % p) == 0)
                return (n == p);
            p = s.next();
        }

        // second, perform t Miller-Rabin tests

        NTL::ZZ x;
        long i;

        for (i = 0; i < t; i++)
        {
            x = NTL::RandomBnd(n); // random number between 0 and n-1

            if (witness(n, x))
                return 0;
        }

        return 1;
    }

    static NTL::ZZ primeGenerate(int bit, NTL::ZZ otherPrime, long testingTimes = ERR)
    {
        NTL::ZZ limitPrime = powerIngeter(NTL::ZZ(2), bit);

        // create odd number
        NTL::ZZ newPrime = RandomBnd(limitPrime);
        if (newPrime % 2 == 0)
            newPrime += 1;

        // store the original generated random number
        NTL::ZZ baseNewPrime = newPrime;
        bool goDown = false;

        // find the prime that satisfy
        // 1. smaller than the pre-defined limit
        // 2. different from the other prime
        while (((!PrimeTest(newPrime, testingTimes)) || (newPrime == otherPrime)))
        {
            // std::cout << newPrime << "-" << otherPrime << std::endl;
            // when the number searching doesn't exceed the limit, just increase by 2 unit
            // otherwise, decrease 2 unit from the original random number
            if ((newPrime < limitPrime) && !goDown)
                newPrime = newPrime + 2;
            else
            {
                if (!goDown)
                {
                    goDown = true;
                    newPrime = baseNewPrime;
                }
                newPrime = newPrime - 2;
            }
        }
        return newPrime;
    }

    // deterministic prime generation
    static NTL::ZZ *maurer_fast_prime(int kbit)
    {
        int timesRepeat = rand() % 7 + 3;
        int timesStop = rand() % (timesRepeat - 1) + 1;

        NTL::ZZ *pair = new NTL::ZZ[2];

        if (kbit < 20)
        {
            NTL::ZZ n = NTL::ZZ(2);
            NTL::ZZ upperBound = NTL::ZZ(powerIngeter(NTL::ZZ(2), kbit));
            NTL::ZZ lowerBound = NTL::ZZ(powerIngeter(NTL::ZZ(2), kbit - 1));
            bool isPrime = true;
            for (n; n <= upperBound; n++)
            {
                isPrime = true;
                NTL::ZZ middleBound = NTL::ZZ(1);
                SqrRoot(middleBound, n);

                for (NTL::ZZ p = NTL::ZZ(2); p <= middleBound; ++p)
                {
                    if (n % p == 0)
                        isPrime = false;
                }
                if ((isPrime) && (n > lowerBound))
                {
                    if (timesRepeat == timesStop)
                    {
                        pair[0] = n;
                        timesRepeat--;
                        continue;
                    }
                    else if (timesRepeat == 0)
                    {
                        break;
                    }
                    else if (timesRepeat > 0)
                    {
                        timesRepeat--;
                    }
                }
            }
            pair[1] = n;
            return pair;
        }
        else
        {
            float c = 0.2f;
            int m = 20;
            NTL::ZZ n = NTL::ZZ(0);

            double B = c * kbit * kbit;
            double r = 0.0;
            if (kbit > 2 * m)
            {
                while (kbit - r * kbit <= m)
                {
                    float s = (float)(rand() % 100 + 1) / 100;
                    r = pow(2, s - 1);
                }
            }
            else
                r = 0.5;

            NTL::ZZ *qTemp = maurer_fast_prime(floor(r * kbit));
            // int q = (int)floor(r * kbit) + 1;
            int q = NTL::conv<int>(*qTemp);

            NTL::ZZ I = (powerIngeter(NTL::ZZ(2), kbit - 1) / (2 * q));
            bool maybePrime = false;
            bool success = false;
            NTL::ZZ R = I + 1;
            number qi = number(NTL::ZZ(q));

            while (!success)
            {
                number Ri = number(R);
                number ni = (number("2") * Ri) * qi;
                NTL::ZZ temp(NTL::INIT_VAL, ni.get().c_str());
                n = temp + 1;

                maybePrime = true;
                for (int p = 2; p < B; ++p)
                {
                    if (n % p == 0)
                        maybePrime = false;
                }
                if (!maybePrime)
                {
                    R++;
                    continue;
                }

                for (NTL::ZZ a = NTL::ZZ(2); a < n - 2; a++)
                {
                    NTL::ZZ b = powerModBigIngeter(a, n - 1, n);
                    if (b == 1)
                    {
                        b = powerModBigIngeter(a, 2 * R, n);

                        NTL::ZZ d = gcd(b - 1, n);
                        if (d == 1)
                        {
                            timesRepeat--;
                            if (timesRepeat == timesStop)
                            {
                                pair[0] = n;
                                continue;
                            }
                            if (timesRepeat == 0)
                            {
                                success = true;
                                pair[1] = n;
                                break;
                            }
                            else
                                break;
                        }
                    }
                }
                if (!success)
                {
                    R++;
                    if (R > 2 * I)
                    {
                        std::cout << "Cannot generate an appropriate prime." << std::endl;
                        break;
                    }
                }
            }
            return pair;
        }
    }

    // Finding other part of public key
    static NTL::ZZ publicKeyE(NTL::ZZ totient)
    {
        NTL::ZZ encrypt = NTL::ZZ(2);
        NTL::ZZ randomCo = NTL::RandomBits_ZZ(8);
        while (encrypt < totient)
        {
            // e must be co-prime to phi and smaller than phi.
            if (gcd(encrypt, totient) == 1)
            {
                if (randomCo == 0)
                    break;
                randomCo--;
            }

            encrypt++;
        }
        return encrypt;
    }

    // Finding other part of private key
    static NTL::ZZ privateKeyD(NTL::ZZ totient, NTL::ZZ encrypt)
    {
        NTL::ZZ a[2] = {NTL::ZZ(0), totient};
        NTL::ZZ b[2] = {NTL::ZZ(1), encrypt};
        while (true)
        {
            if (b[1] == 0)
                return NTL::ZZ(0);
            if (b[1] == 1)
                break;
            NTL::ZZ q = a[1] / b[1];
            NTL::ZZ t[2] = {a[0] - q * b[0], a[1] % b[1]};

            for (int index = 0; index < 2; index++)
            {
                a[index] = b[index];
                b[index] = t[index];
            }
        }
        if (b[0] > 0)
            return b[0];
        else
            return totient + b[0];
    }

    static KeyPair genKeyPair(const unsigned long nbits, int version)
    {
        // NTL::ZZ p, q, tmp, n, e, d, phi_n;
        NTL::ZZ p, q, n, e, d, phi_n;
        srand(time(NULL));
        if (version == 1)
        {
            p = RSA::primeGenerate(nbits, NTL::ZZ(1));
            q = RSA::primeGenerate(nbits, p);
        }
        else if (version == 2)
        {
            NTL::ZZ *pairOfKey = maurer_fast_prime(nbits);
            p = *pairOfKey;
            q = *(pairOfKey + 1);
            std::cout << *pairOfKey << "\t" << q << std::endl;
        }

        n = p * q;
        phi_n = (p - 1) * (q - 1);
        e = RSA::publicKeyE(phi_n);
        // NTL::XGCD(tmp, d, tmp, e, phi_n);
        d = RSA::privateKeyD(phi_n, e);

        return KeyPair{
            PubKey{e, n},
            PvtKey{d, n},
        };
    }

    // EB = 00 || 02 || PS || 00 || D
    static vector<uint8_t> doEncryptPKCS1(const PubKey &pubKey, vector<uint8_t> message)
    {
#ifdef DEBUG_LIB
        cout << "encrypt process" << endl;
#endif
        vector<uint8_t> ret(0);
        long message_len = message.size();
        long key_len = NTL::NumBytes(pubKey.n);
        vector<uint8_t> block(key_len);
        long cur_index = 0;
        unsigned char *array = new unsigned char[block.size()];
        while (message_len > 0)
        {
            long process_len = 0;
            if (message_len + 11 > key_len)
            {
                if (key_len >= 11)
                    process_len = key_len - 11;
                else
                    process_len = key_len;
            }
            else
            {
                process_len = message_len;
            }
            // std::cout << process_len << std::endl;
            long ps_len = key_len - 3 - process_len;
            block[0] = 0x00;
            block[1] = 0x02;
            for (int i = 2; i < ps_len + 2;)
            {
                block[i] = NTL::RandomBnd(UCHAR_MAX);
                if (block[i] != 0x00)
                {
                    i++;
                }
            }
            block[ps_len + 2] = 0x00;
            for (int i = ps_len + 3; i < key_len; i++)
            {
                block[i] = message[cur_index + i - (ps_len + 3)];
            }
            std::copy(block.begin(), block.end(), array);
            reverse_array(array, key_len);
            NTL::ZZ cipher_block = NTL::ZZFromBytes((const unsigned char *)array, key_len);
#ifdef DEBUG_LIB
            cout << "ps_len: " << ps_len << endl;
            cout << "cipher block: " << cipher_block << std::endl;
#endif
            NTL::ZZ cipher = powerModBigIngeter(cipher_block, pubKey.e, pubKey.n);
#ifdef DEBUG_LIB
            cout << "cipher: " << cipher << endl;
#endif
            NTL::BytesFromZZ(array, cipher, key_len);
            for (int i = 0; i < key_len; i++)
            {
                ret.push_back(array[i]);
            }
            cur_index += process_len;
            message_len -= process_len;
            // std::cout << message_len << "\t" << process_len << std::endl;
        }
        delete[] array;
        return ret;
    }

    static vector<uint8_t> doDecryptPKCS1(const PvtKey &pvtKey, vector<uint8_t> cipher_message)
    {
#ifdef DEBUG_LIB
        cout << "decrypt process" << endl;
#endif
        vector<uint8_t> ret;
        long cipher_message_len = cipher_message.size();
        long key_len = NTL::NumBytes(pvtKey.n);
        long current_index = 0;
        unsigned char *array = new unsigned char[key_len];
        while (current_index < cipher_message_len)
        {
            std::copy(cipher_message.begin() + current_index, cipher_message.begin() + current_index + key_len, array);
            NTL::ZZ cipher = NTL::ZZFromBytes(array, key_len);
#ifdef DEBUG_LIB
            cout << "cipher: " << cipher << endl;
#endif
            NTL::ZZ cipher_block = powerModBigIngeter(cipher, pvtKey.d, pvtKey.n);
#ifdef DEBUG_LIB
            cout << "cipher block: " << cipher_block << endl;
#endif
            NTL::BytesFromZZ(array, cipher_block, key_len);
            reverse_array(array, key_len);
            if (array[0] != 0x00 || array[1] != 0x02)
            {
                break;
            }
            int i = 2;
            for (; (i < key_len && array[i] != 0x00); i++)
                ;
            if (array[i] != 0x00)
            {
                break;
            }
            i++;
            for (; i < key_len; i++)
            {
                ret.push_back(array[i]);
            }
            current_index += key_len;
        }
        delete[] array;
        return ret;
    }

    static vector<uint8_t> doEncrypt(const PubKey &pubKey, vector<uint8_t> message, PaddingMode padding)
    {
        if (padding == PaddingMode::PKCS1)
        {
            return doEncryptPKCS1(pubKey, message);
        }
        return vector<uint8_t>(0);
    }

    static vector<uint8_t> doDecrypt(const PvtKey &pvtKey, vector<uint8_t> cipher_message, PaddingMode padding)
    {
        if (padding == PaddingMode::PKCS1)
        {
            return doDecryptPKCS1(pvtKey, cipher_message);
        }
        return vector<uint8_t>(0);
    }
};

#endif