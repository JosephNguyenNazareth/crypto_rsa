# RSA Implementation

> A assignment of Cryptography and Network Security course CO3069 in Ho Chi Minh University of Technology, Vietnam.

## Authors
  * Nguyễn Minh Khôi - 1611657
  * Nguyễn Huỳnh Thoại - 1613379
  * Trần Thanh Lộc - 1611910
  * Lý Hoàng Đang -  1410740

## Prerequisites

* C++ Compiler: `g++`
* NTL Library :  You can download it [here](https://www.shoup.net/ntl/download.html), and follow below for the installation
  * For unix/linux OS: [here](https://www.shoup.net/ntl/doc/tour-unix.html)
  * For window OS : [here](https://www.shoup.net/ntl/doc/tour-win.html)

## RSA Theory 
  * You can find the paper [here](./refs/RSA.pdf)

## Deterministic Prime Generation
* Fast-Maurer Algorithm
* You can find the paper of this algorithm [here](./refs/Maurer.pdf)

## Run

```sh
$ cd srcs
$ make
$ ./rsa 1 # for option 1: probabilistic prime generation
$ ./rsa 2 # for option 2: deterministic prime generation
```

## Library interface

* Import rsa library

```cpp
#include "rsa.hpp"
```

* Create Rsa KeyPair

```cpp
const KeyPair kp = RSA::genKeyPair(key_size, version);
```

* Encrypt a string

```cpp
vector<uint8_t> encrypted_message = RSA::doEncrypt(kp.pubKey,src_msg_vec, PaddingMode::PKCS1);
```


* Decrypt

```cpp
vector<uint8_t> decrypted_message = RSA::doDecrypt(kp.pvtKey, encrypted_message, PaddingMode::PKCS1);
    dest_msg.assign(decrypted_message.begin(), decrypted_message.end());
```


